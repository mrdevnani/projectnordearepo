package com.nordea.util;

import static com.nordea.util.NordeaConstants.CSV_EXTENSION;
import static com.nordea.util.NordeaConstants.XML_EXTENSION;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class NordeaFileConversionUtilTest {
	private static String inputFile1;
	private static String inputFile2;
	private static final String expectedCsvFile = "test_output_csv.csv";
	private static final String expectedXmlFile = "test_output_xml.xml";
	private static final String actualFile1 = "C:\\Users\\Lenovo\\Downloads\\sample_data\\out1";
	private static final String actualFile2 = "C:\\Users\\Lenovo\\Downloads\\sample_data\\out2";

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		inputFile1 = fetchInputFile("test_input_file1.txt");
		inputFile2 = fetchInputFile("test_input_file2.txt");
	}

	private static String fetchInputFile(String name) {
		URL resource = NordeaFileConversionUtilTest.class.getClassLoader().getResource(name);
		if (resource == null) {
			return null;
		} else {
			return new File(resource.getFile()).getPath();
		}
	}

	@Test
	void testConvertFileFormats() throws IOException {
		NordeaFileConversionUtil.convertFileFormats(inputFile1, actualFile1);
		NordeaFileConversionUtil.convertFileFormats(inputFile2, actualFile2);
		assertTrue(isEqual(Paths.get(fetchInputFile(expectedXmlFile)), Paths.get(actualFile1 + XML_EXTENSION)));
		assertTrue(isEqual(Paths.get(fetchInputFile(expectedXmlFile)), Paths.get(actualFile2 + XML_EXTENSION)));
		assertTrue(isEqual(Paths.get(fetchInputFile(expectedCsvFile)), Paths.get(actualFile1 + CSV_EXTENSION)));
		assertTrue(isEqual(Paths.get(fetchInputFile(expectedCsvFile)), Paths.get(actualFile2 + CSV_EXTENSION)));
	}

	private static boolean isEqual(Path firstFile, Path secondFile) {
		try {
			long size = Files.size(firstFile);
			if (size != Files.size(secondFile)) {
				return false;
			}

			if (size < 2048) {
				return Arrays.equals(Files.readAllBytes(firstFile), Files.readAllBytes(secondFile));
			}

			// Compare character-by-character
			try (BufferedReader bf1 = Files.newBufferedReader(firstFile);
					BufferedReader bf2 = Files.newBufferedReader(secondFile)) {

				int ch;
				while ((ch = bf1.read()) != -1) {
					if (ch != bf2.read()) {
						return false;
					}
				}
			}

			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * @Test void testWriteToFile() { // fail("Not yet implemented"); }
	 * 
	 * @Test void testTextToCSV() { // fail("Not yet implemented"); }
	 * 
	 * @Test void testJaxbObjectToXML() { // fail("Not yet implemented"); }
	 * 
	 * @Test void testReadSentences() { // fail("Not yet implemented"); }
	 */

	@AfterAll
	private static void tearDownAfterClass() throws Exception {
		assertTrue(deleteOutputFiles());
	}

	private static boolean deleteOutputFiles() {
		// deleteIfExists File
		try {
			return (Files.deleteIfExists(Paths.get(actualFile1 + XML_EXTENSION))
					&& Files.deleteIfExists(Paths.get(actualFile2 + XML_EXTENSION))
					&& Files.deleteIfExists(Paths.get(actualFile1 + CSV_EXTENSION))
					&& Files.deleteIfExists(Paths.get(actualFile2 + CSV_EXTENSION)));
		} catch (IOException e) {
			return false;
		}
	}
}
