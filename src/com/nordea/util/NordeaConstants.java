package com.nordea.util;

public class NordeaConstants {
	public static final String REGEX_SENTENCE = "(?<=[.?!])\\s*";
	public static final String SPACE = " ";
	public static final String SENTENCE = "Sentence";
	public static final String WORD = "Word";
	public static final String NEWLINE = "\n";
	public static final String COMMA = ",";
	public static final String EMPTY = "";
	public static final String REGEX_SPECIAL_CHARACTERS = "[!-?*#(.*)\\>]";
	public static final String REGEX_LINE_BREAKS = "\r\n";
	public static final String CSV_EXTENSION = ".csv";
	public static final String XML_EXTENSION = ".xml";
}
