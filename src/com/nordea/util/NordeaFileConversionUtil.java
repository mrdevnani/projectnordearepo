/**
 * 
 */
package com.nordea.util;

import static com.nordea.util.NordeaConstants.COMMA;
import static com.nordea.util.NordeaConstants.CSV_EXTENSION;
import static com.nordea.util.NordeaConstants.NEWLINE;
import static com.nordea.util.NordeaConstants.REGEX_LINE_BREAKS;
import static com.nordea.util.NordeaConstants.REGEX_SENTENCE;
import static com.nordea.util.NordeaConstants.SENTENCE;
import static com.nordea.util.NordeaConstants.SPACE;
import static com.nordea.util.NordeaConstants.XML_EXTENSION;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Mayur Devnani
 * 
 *         This class is used to convert text file in CSV and/or XML
 *
 */
public class NordeaFileConversionUtil {

	
	private static final Log log = LogFactory.getLog(NordeaFileConversionUtil.class);
	private static final String inputFilename = "C:\\Users\\Lenovo\\Downloads\\sample_data\\small.in";
	private static final String outputFilenameWithoutExtension = "C:\\Users\\Lenovo\\Downloads\\sample_data\\small-output";

	public static void main(String[] args) {
		// String inputFilename = args[0];
		// String outputFilenameWithoutExtension = args[1];
		convertFileFormats(inputFilename, outputFilenameWithoutExtension);
	}

	/**
	 * Converts input file to xml & csv formats
	 */
	public static void convertFileFormats(String inputFilename, String outputFilenameWithoutExtension) {
		List<Sentence> sentences = readSentences(inputFilename);
		if (sentences != null && !sentences.isEmpty()) {
			StringBuilder sb = jaxbObjectToXML(sentences);
			writeToFile(outputFilenameWithoutExtension, sb, XML_EXTENSION);
			sb = textToCSV(sentences);
			writeToFile(outputFilenameWithoutExtension, sb, CSV_EXTENSION);
		}
	}

	/**
	 * Write contents to output file
	 * 
	 * @param outputFile
	 * @param sb
	 * @param extension
	 */
	public static void writeToFile(String outputFile, StringBuilder sb, String extension) {
		log.info("Writing " + extension + " file...");
		if (sb != null) {
			try {
				Files.write(Paths.get(outputFile + extension), sb.toString().getBytes());
			} catch (IOException e) {
				log.warn("Error writing output file: " + e.getMessage(), e);
				e.printStackTrace();
			}
		} else {
			log.warn("Nothing to write, output file not created.");
		}

	}

	/**
	 * Convert sentences into CSV format
	 * 
	 * @param sentences
	 * @return
	 */
	public static StringBuilder textToCSV(List<Sentence> sentences) {
		log.info("Converting sentences into csv format...");
		StringBuilder sb = new StringBuilder(COMMA);
		int maxWordLength = sentences.stream().mapToInt(s -> s.getWords().size()).max().getAsInt();
		for (int i = 1; i <= maxWordLength; i++) {
			sb.append(NordeaConstants.WORD + SPACE + i + COMMA + SPACE);
		}
		sb.deleteCharAt(sb.lastIndexOf(COMMA)).append(NEWLINE);
		AtomicInteger count = new AtomicInteger(0);
		sentences.forEach(s -> {
			sb.append(SENTENCE + SPACE + count.incrementAndGet() + COMMA);
			sb.append(String.join(COMMA, s.getWords()));
			sb.append(NEWLINE);
		});
		return sb;
	}

	/**
	 * Convert sentences into XML format using JAXB
	 * 
	 * @param sentences
	 * @return
	 */
	public static StringBuilder jaxbObjectToXML(List<Sentence> sentences) {
		log.info("Converting sentences into xml format...");
		StringWriter sw = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Sentences.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			Sentences multipleSentences = new Sentences();
			sentences.forEach(s -> multipleSentences.add(s));
			jaxbMarshaller.marshal(multipleSentences, sw);
		} catch (JAXBException e) {
			log.error("Error converting to xml file " + e.getMessage() + e);
		}
		return new StringBuilder(sw.getBuffer());
	}

	/**
	 * Read Sentences from given input file
	 * 
	 * @param filename
	 * @return - list of Sentence object
	 */
	public static List<Sentence> readSentences(String filename) {
		log.info("Generating sentences by reading input file...");
		StringBuilder sb = new StringBuilder();
		try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename))) {
			reader.lines().filter(l -> !l.isEmpty()).forEach(l -> {
				l = NordeaUtils.handleSalutations(l, false, false);
				sb.append(l.replaceAll(REGEX_LINE_BREAKS, SPACE));
			});
		} catch (IOException e) {
			log.error("Error reading input file " + filename, e);
		}

		return Arrays.stream(sb.toString().split(REGEX_SENTENCE)).map(s -> new Sentence(s)).filter(s -> !s.isEmpty())
				.collect(Collectors.toList());
	}

}
