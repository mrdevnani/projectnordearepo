package com.nordea.util;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "text")
public class Sentences {

	private List<Sentence> sentences;

	public List<Sentence> getSentences() {
		return sentences;
	}

	@XmlElement(name = "sentence")
	public void setSentences(List<Sentence> sentences) {
		this.sentences = sentences;
	}

	public void add(Sentence sentence) {
		if (this.sentences == null) {
			this.sentences = new ArrayList<>();
		}
		this.sentences.add(sentence);
	}
}
