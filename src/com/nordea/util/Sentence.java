package com.nordea.util;

import static com.nordea.util.NordeaConstants.COMMA;
import static com.nordea.util.NordeaConstants.EMPTY;
import static com.nordea.util.NordeaConstants.REGEX_SPECIAL_CHARACTERS;
import static com.nordea.util.NordeaConstants.SPACE;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class Sentence {

	private String sentence;
	private List<String> words;

	public Sentence() {
	}

	public Sentence(String sentence) {
		this.sentence = sentence;
		this.words = getWords(sentence);
	}

	/**
	 * Breaks sentence into list of sorted words after trimming special characters
	 * 
	 * @param sentence
	 * @return - words list
	 */
	public List<String> getWords(String sentence) {
		return Arrays.stream(sentence.replaceAll(COMMA, SPACE).trim().split("\\s+"))
				.map(s -> s.replaceAll(REGEX_SPECIAL_CHARACTERS, EMPTY).trim()).filter(s -> !s.isEmpty())
				.sorted((o1, o2) -> o1.compareToIgnoreCase(o2)).map(s -> NordeaUtils.handleSalutations(s, true, true))
				.collect(Collectors.toList());
	}

	@XmlTransient
	public String getSentence() {
		return sentence;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	@XmlElement(name = "word")
	public List<String> getWords() {
		return words;
	}

	public void setWords(List<String> words) {
		this.words = words;
	}

	@Override
	public String toString() {
		return this.sentence;
	}

	public boolean isEmpty() {
		return this.sentence == null || this.words.isEmpty();

	}

}
