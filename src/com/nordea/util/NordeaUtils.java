package com.nordea.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class NordeaUtils {

	private static Map<String, String> staticSalutationsMap;
	private static Map<String, String> staticReverseSalutationsMap;

	/**
	 * Handle Salutations for a given String/line/sentence
	 * 
	 * @param s
	 * @param useEquals  - if true, check equals condition, else check contains
	 *                   condition
	 * @param useReverse - if false, fetches staticSalutationsMap else if true
	 *                   fetches staticReverseSalutationsMap
	 * @return
	 */
	public static String handleSalutations(String s, boolean useEquals, boolean useReverse) {
		Map<String, String> map = getSalutationsMap(useReverse);
		Optional<String> key = map.keySet().parallelStream().filter(useEquals ? s::equals : s::contains).findFirst();
		if (key.isPresent()) {
			return s.replaceAll(key.get(), map.get(key.get()));
		}
		return s;
	}

	public static Map<String, String> getSalutationsMap(boolean reverse) {
		if (reverse) {
			if (staticReverseSalutationsMap != null) {
				return staticReverseSalutationsMap;
			} else {
				return staticReverseSalutationsMap = getSalutationsMap().entrySet().stream()
						.collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
			}
		}
		return getSalutationsMap();
	}

	public static final Map<String, String> getSalutationsMap() {
		if (staticSalutationsMap != null) {
			return staticSalutationsMap;
		}
		staticSalutationsMap = new HashMap<String, String>();
		staticSalutationsMap.put("Mr.", "Mr");
		staticSalutationsMap.put("Mrs.", "Mrs");
		staticSalutationsMap.put("Ms.", "Ms");
		staticSalutationsMap.put("Dr.", "Dr");
		staticSalutationsMap.put("Jr.", "Jr");
		staticSalutationsMap.put("Sr.", "Sr");
		return staticSalutationsMap;
	}
}
